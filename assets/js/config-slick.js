$(document).ready(function () {
    $('.slider-home').slick({
        dots: true,
        fade: true,
        arrows: true,
        //autoplay:true,
        autoplaySpeed: 2500,
        speed: 900,
        infinite: true,
        //cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
        //touchThreshold: 100
        prevArrow: '<img class="btn-carousel btn-prev" src="assets/images/icons/arrow-prev-home.svg">',
        nextArrow: '<img class="btn-carousel btn-next" src="assets/images/icons/arrow-next-home.svg">',
    });
});

$(document).ready(function () {
    $('.carousel-testimonials').slick({
        dots: true,
        infinite: true,
        centerMode: true,
        slidesToShow: 1,
        //slidesToScroll: 3,
        centerPadding: '9.5em',
    })
});

$(document).ready(function () {
    $('.content-card-specialties').slick({
        dots: true,
        autoplay: false,
        autoplaySpeed: 4000,
        infinite: true,
        arrows: false,
        rows: 2,
        slidesPerRow: 5,
    })

});
$(document).ready(function () {
/*$('.carousel_').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '0px',
        //autoplay: true,
        //autoplaySpeed: 10000,
        responsive: [{
            breakpoint: 1024,
            settings: {
                centerMode: false,
                arrows: false,
                slidesToShow: 1.25,
                infinite: false,
            }
        }]
    });*/
    $('.center').slick({
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 3,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 1
            }
          }
        ]
      });
});